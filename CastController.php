<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view ('casts.create');
    }

    public function store (Request $request){
        //dd($request->all());
        $request -> validate ([
            'nama' => 'required|unique:casts',
            'umur' => 'required|unique:casts',
            'bio' => 'required'
        ]);

        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],           
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/profiles/create');
    }

    public function index(){
        $casts = DB::table('casts')->get(); //select * from casts
        //dd ($casts);
        return view ('casts.index');
    }

}
