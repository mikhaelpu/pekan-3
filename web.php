<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
route::get('/tes', function (){
    return view('test');
});

route::get('/test/{angka}', function ($angka){
    return view('test', ["angka" => $angka ]);
});

route::get('/halo/{nama}', function ($nama){
    return "Halo $nama";
});

route::get('/form', 'RegisterController@form');

route::get('/sapa', 'RegisterController@sapa');
route::post('/sapa', 'RegisterController@sapa_post');

//Tugas Lavarel Web Statis

route::get('/home', function(){
    return view('home');
});

route::get('/home', 'HomeController@home');

route::get('/register', 'AuthController@register');
//route::post('/register', 'AuthController@register_post');

route::get('/wellcome', 'AuthController@wellcome');

// Tugas Template Laravel

route::get('/master', function (){
    return view('master');
});
*/

// Berlatih CRUD di Laravel

route::get('/profiles/create', 'ProfileCOntroller@create');
route::post('/profiles', 'ProfileController@store');

// Tugas erlatih CRUD di Laravel

route::get('/casts/create', 'CastController@create');
route::post('/casts', 'CastController@store');
route::get('/casts', 'CastController@index');
