<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmIdToKritiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kritiks', function (Blueprint $table) {
            $table->unsignedBigInteger('film_id');

            $table->foreign('film_id')->references('id')->on('films');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kritiks', function (Blueprint $table) {
            $table->dropforeign(['film_id']);
            $table->dropcoloumn(['film_id']);
        });
    }
}
